package ua.nure.beizerov.practice7.util;


import java.util.Collections;
import java.util.Comparator;

import ua.nure.beizerov.practice7.constants.Constants;
import ua.nure.beizerov.practice7.controller.DOMController;
import ua.nure.beizerov.practice7.entity.Aircraft;
import ua.nure.beizerov.practice7.entity.Plane;


/**
 * Contains static methods for sorting.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Sorter {
	// //////////////////////////////////////////////////////////
	// these are comparators
	// //////////////////////////////////////////////////////////

	/**
	 * Sorts planes by model
	 */
	public static final Comparator<Plane> SORT_PLANES_BY_MODEL;

	/**
	 * Sorts planes by origin.
	 */
	public static final Comparator<Plane> SORT_PLANES_BY_ORIGIN;

	/**
	 * Sorts planes by price.
	 */
	public static final Comparator<Plane> SORT_PLANES_BY_PRICE;
	
	
	static {
		SORT_PLANES_BY_MODEL = (Plane p1, Plane p2) ->
			p1.getModel().compareTo(p2.getModel());
		
		SORT_PLANES_BY_ORIGIN = (Plane p1, Plane p2) ->
			p1.getOrigin().compareTo(p2.getOrigin());
		
		SORT_PLANES_BY_PRICE = (Plane p1, Plane p2) ->
			((Double) p1.getPrice()).compareTo(p2.getPrice());
	}
	
	
	// //////////////////////////////////////////////////////////
	// these methods take Aircraft object and sort it
	// with according comparator
	// //////////////////////////////////////////////////////////

	public static final void sortPlanesByModel(Aircraft aircraft) {
		Collections.sort(aircraft.getPlanes(), SORT_PLANES_BY_MODEL);
	}

	public static final void sortPlanesByOrigin(Aircraft aircraft) {
		Collections.sort(aircraft.getPlanes(), SORT_PLANES_BY_ORIGIN);
	}

	public static final void sortPlanesByPrice(Aircraft aircraft) {
		Collections.sort(aircraft.getPlanes(), SORT_PLANES_BY_PRICE);
	}
	
	public static void main(String[] args) throws Exception {
		// Aircraft.xml --> Aircraft object
		DOMController domController = new DOMController(
				Constants.VALID_XML_FILE);
		domController.parse(false);
		Aircraft aircraft = domController.getAircraft();

		System.out.println("====================================");
		System.out.println(aircraft);
		System.out.println("====================================");

		System.out.println("==============sortPlanesByModel==================");
		Sorter.sortPlanesByModel(aircraft);
		System.out.println(aircraft);
		System.out.println("====================================");

		System.out.println("==============sortPlanesByPrice==================");
		Sorter.sortPlanesByPrice(aircraft);
		System.out.println(aircraft);
	}
}
