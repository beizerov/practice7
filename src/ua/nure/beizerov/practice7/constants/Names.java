package ua.nure.beizerov.practice7.constants;


/**
 * Holds entities declared in XSD document.
 * 
 * @author Alexei Beizerov
 */
public final class Names {
	
	private Names() {
		throw new IllegalStateException("Names class");
	}

	public static final String AIRCRAFT = "Aircraft";

	public static final String PLANE = "Plane";
	
	public static final String MODEL = "Model";
	
	public static final String ORIGIN = "Origin";

	public static final String PLACES = "places";
	
	public static final String CHARS = "Chars";
	
	public static final String AMMUNITION = "Ammunition";
	
	public static final String MISSILE = "missile";

	public static final String PARAMETERS = "Parameters";
	
	public static final String LENGTH = "length";
	
	public static final String WIDTH = "width";
	
	public static final String HEIGHT = "height";
	
	public static final String PRICE = "Price";
	
	public static final String SPECIFICATIONS = "Specifications";

	public static final String RADAR = "radar";
}
