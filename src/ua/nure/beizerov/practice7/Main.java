package ua.nure.beizerov.practice7;


import ua.nure.beizerov.practice7.controller.DOMController;
import ua.nure.beizerov.practice7.controller.SAXController;
import ua.nure.beizerov.practice7.controller.STAXController;
import ua.nure.beizerov.practice7.entity.Aircraft;
import ua.nure.beizerov.practice7.util.Sorter;


/**
 * This class is represent a Main.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Main {
	public static void usage() {
		System.out.println("java ua.nure.test.practice7.Main xmlFileName");
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			usage();
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get
		DOMController domController = new DOMController(xmlFileName);
		domController.parse(true);
		Aircraft aircraft = domController.getAircraft();

		// sort (case 1)
		Sorter.sortPlanesByModel(aircraft);
		
		// save
		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXML(aircraft, outputXmlFile);
		System.out.println("Output ==> " + outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse(true);
		aircraft = saxController.getAircraft();
		
		// sort  (case 2)
		Sorter.sortPlanesByOrigin(aircraft);
		
		// save
		outputXmlFile = "output.sax.xml";
		
		// other way: 
		DOMController.saveToXML(aircraft, outputXmlFile);
		System.out.println("Output ==> " + outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		aircraft = staxController.getAircraft();
		
		// sort  (case 3)
		Sorter.sortPlanesByPrice(aircraft);
		
		// save
		outputXmlFile = "output.stax.xml";
		DOMController.saveToXML(aircraft, outputXmlFile);
		System.out.println("Output ==> " + outputXmlFile);
	}
}
