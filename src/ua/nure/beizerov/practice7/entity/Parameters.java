package ua.nure.beizerov.practice7.entity;


/**
 * Implements the Parameters entity.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Parameters {

    protected double length;
    protected double width;
    protected double height;


    public double getLength() {
        return length;
    }

    public void setLength(double value) {
        this.length = value;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double value) {
        this.width = value;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double value) {
        this.height = value;
    }

	@Override
	public String toString() {
		return "Parameters: [length = " + length + ", width = "
				+ width + ", height = " + height + "]";
	}
}
