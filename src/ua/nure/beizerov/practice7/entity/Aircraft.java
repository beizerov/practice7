package ua.nure.beizerov.practice7.entity;


import java.util.ArrayList;
import java.util.List;


/**
 * Implements the Aircraft entity.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Aircraft {

    private List<Plane> planes;

    public List<Plane> getPlanes() {
        if (planes == null) {
            planes = new ArrayList<>();
        }
        
        return this.planes;
    }

	@Override
	public String toString() {
		if (planes == null || planes.isEmpty()) {
			return "Aircraft contains no planes";
		}
		
		StringBuilder result = new StringBuilder();
		
		for (Plane plane : planes) {
			result.append(plane).append('\n');
		}
		return result.toString();
	}
}
