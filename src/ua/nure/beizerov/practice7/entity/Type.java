package ua.nure.beizerov.practice7.entity;


/**
 * Implements the Type entities.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public enum Type {
	SUPPORT("Support"),
	ESCORT("Escort"),
	FIGHTER("Fighter"),
	INTERCEPTOR("Interceptor"),
	SCOUT("Scout");
	
	Type(String type) {
		this.role = type;
	}
	
	private String role;

	public String getRole() {
		return role;
	}
}
