package ua.nure.beizerov.practice7.entity;


/**
 * Implements the Chars entity.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Chars {
	
	private Type type;

    private int missiles;
    
    private int places = 1;

	private Boolean radar;
    
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
    public int getMissiles() {
		return missiles;
	}

	public void setMissiles(int missiles) {
		this.missiles = missiles;
	}

	public int getPlaces() {
		return places;
	}

	public void setPlaces(int places) {
		this.places = places;
	}
	
    public boolean isRadar() {
        if (radar == null) {
            return false;
        } else {
            return radar;
        }
    }

    public void setRadar(Boolean value) {
        this.radar = value;
    }

	@Override
	public String toString() {
		String strRadar = (isRadar()) ? (", radar=" + radar) : "";
		String strMissiles = (getType() == Type.SCOUT) 
								? (", missiles = " + missiles) 
								: ""
		;
		
		return "Chars: [" + type + strMissiles
				+ ", places = " + places + strRadar + "]";
	}
}
