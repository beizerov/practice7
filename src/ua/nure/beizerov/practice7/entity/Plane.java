package ua.nure.beizerov.practice7.entity;


/**
 * Implements the Plane entity.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Plane {

    private String model;
    private String origin;
    private Chars chars;
    private Parameters parameters;
    private double price;


    public String getModel() {
        return model;
    }

    public void setModel(String value) {
        this.model = value;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String value) {
        this.origin = value;
    }

    public Chars getChars() {
        return chars;
    }

    public void setChars(Chars value) {
        this.chars = value;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters value) {
        this.parameters = value;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double value) {
        this.price = value;
    }

	@Override
	public String toString() {
		return "Plane\nModel: " + model + "\nOrigin: " 
				+ origin + "\n" + chars + "\n" + parameters
				+ "\nprice: " + price + "\n";
	}
    
    
}
