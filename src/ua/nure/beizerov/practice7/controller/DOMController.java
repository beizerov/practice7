package ua.nure.beizerov.practice7.controller;


import java.io.File;
import java.io.IOException;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import ua.nure.beizerov.practice7.constants.Constants;
import ua.nure.beizerov.practice7.constants.Names;
import ua.nure.beizerov.practice7.entity.Aircraft;
import ua.nure.beizerov.practice7.entity.Chars;
import ua.nure.beizerov.practice7.entity.Parameters;
import ua.nure.beizerov.practice7.entity.Plane;
import ua.nure.beizerov.practice7.entity.Type;



/**
 * Controller for DOM parser.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public class DOMController {
	private String xmlFileName;

	// main container
	private Aircraft aircraft;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	
	public Aircraft getAircraft() {
		return aircraft;
	}
	
	/**
	 * Parses XML document.
	 * 
	 * @param validate
	 *            If true validate XML document against its XML schema.
	 */
	public void parse(boolean validate) 
			throws ParserConfigurationException, SAXException, IOException {

		// obtain DOM parser 
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		// set properties for Factory
		
		// XML document contains namespaces
		dbf.setNamespaceAware(true);
		
		// make parser validating
		if (validate) {
			// turn validation on
			dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			
			// turn on xsd validation 
			dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}
		
		DocumentBuilder db = dbf.newDocumentBuilder();

		// set error handler
		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				// throw exception if XML document is NOT valid
				throw e;
			}
		});

		// parse XML document
		Document document = db.parse(xmlFileName);
		
		// get root element
		Element root = document.getDocumentElement();

		// create container
		aircraft = new Aircraft();

		// obtain planes nodes
		NodeList planeNodes = root
				.getElementsByTagName(Names.PLANE);

		// process planes nodes
		for (int j = 0; j < planeNodes.getLength(); j++) {
			Plane plane = getPlane(planeNodes.item(j));
			// add plane to container
			aircraft.getPlanes().add(plane);
		}
	}
	
	/**
	 * Extracts plane object from the plane XML node.
	 * 
	 * @param pNode
	 *            Plane node.
	 * @return Plane object.
	 */
	private static Plane getPlane(Node pNode) {
		Plane plane = new Plane();
		Element pElement = (Element) pNode;

		// set model
		plane.setModel(
				pElement
					.getElementsByTagName(Names.MODEL)
					.item(0)
					.getTextContent()
		);
		// set origin
		plane.setOrigin(
				pElement
					.getElementsByTagName(Names.ORIGIN)
					.item(0)
					.getTextContent()
		);
		// set chars
		plane.setChars(
				getChars(
						pElement
							.getElementsByTagName(Names.CHARS)
							.item(0))
		);
		// set parameters
		plane.setParameters(
				getParameters(
						pElement
							.getElementsByTagName(Names.PARAMETERS)
							.item(0)
				)
		);
		// set price
		plane.setPrice(Double.parseDouble(
						pElement
							.getElementsByTagName(Names.PRICE)
							.item(0)
							.getTextContent()
						)
		);
		
		return plane;
	}
	
	private static Chars getChars(Node cNode) {
		Chars chars = new Chars();
		Element cElement = (Element) cNode;
		
		String type = "";
		Node nType;
		for (Type t : Type.values()) {
			nType = cElement
					.getElementsByTagName(t.getRole())
					.item(0);
			
			if (nType != null) {
				type = nType.getNodeName();
				break;
			}
		}
		
		chars.setType(Type.valueOf(type.toUpperCase(Locale.ENGLISH)));
		
		if (chars.getType() == Type.SCOUT) {
			int quantity = Integer.parseInt(
						cElement
							.getElementsByTagName(Names.MISSILE)
							.item(0)
							.getTextContent())
			;
			
			chars.setMissiles(quantity);
		}
		
		int places = Integer.parseInt(
						cElement
							.getElementsByTagName(Names.PLACES)
							.item(0)
							.getTextContent()
		);

		chars.setPlaces(places);
		
		if (Boolean.valueOf(cElement.getAttribute(Names.RADAR))) {
			chars.setRadar(
					Boolean.valueOf(cElement.getAttribute(Names.RADAR))
			);	
		}

		return chars;
	}
	
	private static Parameters getParameters(Node pNode) {
		Parameters parameters = new Parameters();
		Element pElement = (Element) pNode;
		
		double length = Double.parseDouble(
							pElement
								.getElementsByTagName(Names.LENGTH)
								.item(0)
								.getTextContent()
		);
		double width = Double.parseDouble(
				pElement
					.getElementsByTagName(Names.WIDTH)
					.item(0)
					.getTextContent()
		);
		double hight = Double.parseDouble(
				pElement
					.getElementsByTagName(Names.HEIGHT)
					.item(0)
					.getTextContent()
		);
		
		parameters.setLength(length);
		parameters.setWidth(width);
		parameters.setHeight(hight);
		
		return parameters;
	}
	
		// //////////////////////////////////////////////////////
		// Static util methods
		// //////////////////////////////////////////////////////

		/**
		 * Creates and returns DOM of the Aircraft container.
		 * 
		 * @param aircraft
		 *            Aircraft object.
		 * @throws ParserConfigurationException 
		 */
		public static Document getDocument(Aircraft aircraft) 
				throws ParserConfigurationException {
		
			// obtain DOM parser
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			// set properties for Factory

			// XML document contains namespaces
			dbf.setNamespaceAware(true);

			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.newDocument();

			// create root element
			Element aircraftElement = document.createElement(Names.AIRCRAFT);

			// add root element
			document.appendChild(aircraftElement);

			// add planes elements
			for (Plane plane : aircraft.getPlanes()) {
				// create plane element
				Element pElement = document.createElement(Names.PLANE);
				// add plane
				aircraftElement.appendChild(pElement);

				// create model element
				Element mElement = document.createElement(Names.MODEL);
				mElement.setTextContent(plane.getModel());
				// add model
				pElement.appendChild(mElement);
				
				// create origin element
				Element oElement = document.createElement(Names.ORIGIN);
				oElement.setTextContent(plane.getOrigin());
				// add origin
				pElement.appendChild(oElement);
				
				// create chars element
				Element cElement = document.createElement(Names.CHARS);
				
				if(plane.getChars().isRadar()) {
					cElement.setAttribute(Names.RADAR, "true");
				}
				
				// create type element
				Element tElement = document.createElement(
					plane
						.getChars()
						.getType()
						.getRole()
				);
				
				if (plane.getChars().getType() == Type.SCOUT) {
					// create missile element
					Element missileElement = document
												.createElement(Names.MISSILE);
					
					missileElement
						.setTextContent(
								String.valueOf(
										plane
											.getChars()
											.getMissiles()
								)
					);
					// add missile to scout
					tElement.appendChild(missileElement);
				}
				// add type to chars
				cElement.appendChild(tElement);
				
				// create places element
				Element plElement = document.createElement(Names.PLACES);
				plElement.setTextContent(
					String.valueOf(plane.getChars().getPlaces())
				);
				// add places to chars
				cElement.appendChild(plElement);
				// add chars
				pElement.appendChild(cElement);
				
				// create parameters element
				Element paramElement = document.createElement(Names.PARAMETERS);
				// create langth element
				Element lElement = document.createElement(Names.LENGTH);
				lElement.setTextContent(
						String.valueOf(
									plane
										.getParameters()
										.getLength()
						)
				);
				// add length to parameters
				paramElement.appendChild(lElement);

				// create width element
				Element wElement = document.createElement(Names.WIDTH);
				wElement.setTextContent(
						String.valueOf(
									plane
										.getParameters()
										.getWidth()
						)
				);
				// add width to parameters
				paramElement.appendChild(wElement);
				
				// create height element
				Element hElement = document.createElement(Names.HEIGHT);
				hElement.setTextContent(
						String.valueOf(
									plane
										.getParameters()
										.getHeight()
						)
				);
				// add height to parameters
				paramElement.appendChild(hElement);
				
				// add parameters
				pElement.appendChild(paramElement);
				
				// create price element
				Element priceElement = document.createElement(Names.PRICE);
				priceElement.setTextContent(String.valueOf(plane.getPrice()));
				// add price
				pElement.appendChild(priceElement);
			}

			return document;		
		}
		
		/**
		 * Saves Aircraft object to XML file.
		 * 
		 * @param aircraft
		 *            Aircraft object to be saved.
		 * @param xmlFileName
		 *            Output XML file name.
		 */
		public static void saveToXML(Aircraft aircraft, String xmlFileName)
				throws ParserConfigurationException, TransformerException {	
			
			// Aircraft -> DOM -> XML
			saveToXML(getDocument(aircraft), xmlFileName);
		}
		
		/**
		 * Save DOM to XML.
		 * 
		 * @param document
		 *            DOM to be saved.
		 * @param xmlFileName
		 *            Output XML file name.
		 */
		public static void saveToXML(Document document, String xmlFileName) 
				throws TransformerException {
			
			StreamResult result = new StreamResult(new File(xmlFileName));
			
			// set up transformation
			TransformerFactory tf = TransformerFactory.newInstance();
			javax.xml.transform.Transformer t = tf.newTransformer();
			t.setOutputProperty(OutputKeys.INDENT, "yes");			
			
			// run transformation
			t.transform(new DOMSource(document), result);
		}
		
		public static void main(String[] args) throws Exception {
			// try to parse NOT valid XML document with validation on (failed)
			DOMController domContr = new DOMController(
					Constants.INVALID_XML_FILE
			);
			try {
				// parse with validation (failed)
				domContr.parse(true);
			} catch (SAXException ex) {
				System.err.println("====================================");
				System.err.println("XML not valid");
				System.err.println(
						"Aircraft object --> " + domContr.getAircraft()
				);
				System.err.println("====================================");
			}

			// try to parse NOT valid XML document with validation off (success)
			domContr.parse(false);

			// we have Aircraft object at this point:
			System.out.println("====================================");
			System.out.print("\t*** Aircraft ***\n" + domContr.getAircraft());
			System.out.println("====================================");

			// save aircraft in XML file
			Aircraft aircraft = domContr.getAircraft();
			DOMController.saveToXML(
					aircraft, Constants.INVALID_XML_FILE + ".dom-result.xml"
			);
		}
}
