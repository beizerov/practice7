package ua.nure.beizerov.practice7.controller;


import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import ua.nure.beizerov.practice7.constants.Constants;
import ua.nure.beizerov.practice7.constants.Names;
import ua.nure.beizerov.practice7.entity.Aircraft;
import ua.nure.beizerov.practice7.entity.Plane;
import ua.nure.beizerov.practice7.entity.Chars;
import ua.nure.beizerov.practice7.entity.Type;
import ua.nure.beizerov.practice7.entity.Parameters;


/**
 * Controller for SAX parser.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	// current element name holder
	private String currentElement;
	
	// main container
	private Aircraft aircraft;
	
	private Plane plane;

	
	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	
	
	/**
	 * Parses XML document.
	 * 
	 * @param validate
	 *            If true validate XML document against its XML schema. With
	 *            this parameter it is possible make parser validating.
	 */
	public void parse(boolean validate) 
			throws ParserConfigurationException, SAXException, IOException {
		
		// obtain sax parser factory
		SAXParserFactory factory = SAXParserFactory.newInstance();

		// XML document contains namespaces
		factory.setNamespaceAware(true);
		
		// set validation
		if (validate) {
			factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, this);
	}

	// ///////////////////////////////////////////////////////////
	// ERROR HANDLER IMPLEMENTATION
	// ///////////////////////////////////////////////////////////

	@Override
	public void error(org.xml.sax.SAXParseException e) throws SAXException {
		// if XML document not valid just throw exception
		throw e;
	}
	
	public Aircraft getAircraft() {
		return aircraft;
	}
	
	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		currentElement = localName;

		if (Names.AIRCRAFT.equals(currentElement)) {
			aircraft = new Aircraft();
			return;
		}

		if (Names.PLANE.equals(currentElement)) {
			plane = new Plane();
			return;
		}

		if (Names.CHARS.equals(currentElement)) {
			plane.setChars(new Chars());
			plane.getChars().setRadar(
					Boolean.parseBoolean(attributes.getValue(uri, Names.RADAR))
			);
			
			return;
		}
		
		switch (currentElement) {
		case "Support":
			plane.getChars().setType(Type.SUPPORT);
			break;
		case "Escort":
			plane.getChars().setType(Type.ESCORT);
			break;
		case "Fighter":
			plane.getChars().setType(Type.FIGHTER);
			break;
		case "Interceptor":
			plane.getChars().setType(Type.INTERCEPTOR);
			break;
		case "Scout":
			plane.getChars().setType(Type.SCOUT);
			break;
		default:
			break;
		}
		
		if (Names.PARAMETERS.equals(currentElement)) {
			plane.setParameters(new Parameters());
			return;
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		String elementText = new String(ch, start, length).trim();

		// return if content is empty
		if (elementText.isEmpty()) { 
			return;
		}

		if (Names.MODEL.equals(currentElement)) {
			plane.setModel(elementText);
			return;
		}
		
		if (Names.ORIGIN.equals(currentElement)) {
			plane.setOrigin(elementText);
			return;
		}
		
		if (Names.MISSILE.equals(currentElement)) {
			plane.getChars().setMissiles((Integer.parseInt(elementText)));
			return;
		}
		
		if (Names.PLACES.equals(currentElement)) {
			plane.getChars().setPlaces((Integer.parseInt(elementText)));
			return;
		}
		
		if (Names.LENGTH.equals(currentElement)) {
			plane.getParameters().setLength((Double.parseDouble(elementText)));
			return;
		}
		
		if (Names.WIDTH.equals(currentElement)) {
			plane.getParameters().setWidth((Double.parseDouble(elementText)));
			return;
		}
		
		if (Names.HEIGHT.equals(currentElement)) {
			plane.getParameters().setHeight((Double.parseDouble(elementText)));
			return;
		}
		
		if (Names.PRICE.equals(currentElement)) {
			plane.setPrice(Double.parseDouble(elementText));
			return;
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		if (Names.PLANE.equals(localName)) {
			// just add plane to container
			aircraft.getPlanes().add(plane);
			return;
		}
	}
	
	public static void main(String[] args) throws Exception {
		// try to parse valid XML file (success)
		SAXController saxContr = new SAXController(Constants.VALID_XML_FILE);
		
		// do parse with validation on (success)
		saxContr.parse(true);
		
		// obtain container
		Aircraft aircraft = saxContr.getAircraft();

		// we have Aircraft object at this point:
		System.out.println("====================================");
		System.out.print("\t*** Aircraft ***\n" + aircraft);
		System.out.println("====================================");

		// now try to parse NOT valid XML (failed)
		saxContr = new SAXController(Constants.INVALID_XML_FILE);
		try {			
			// do parse with validation on (failed)
			saxContr.parse(true);
		} catch (Exception ex) {
			System.err.println("====================================");
			System.err.println("Validation is failed:\n" + ex.getMessage());
			System.err.println(
					"Try to print aircraft object : " + saxContr.getAircraft()
			);
			System.err.println("====================================");
		}

		// and now try to parse NOT valid XML with validation off (success)
		saxContr.parse(false);		

		// we have Aircraft object at this point:
		System.out.println("====================================");
		System.out.print("\t*** Aircraft ***\n" + saxContr.getAircraft());
		System.out.println("====================================");
	}
}
