package ua.nure.beizerov.practice7.controller;


import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.helpers.DefaultHandler;

import ua.nure.beizerov.practice7.constants.Constants;
import ua.nure.beizerov.practice7.constants.Names;
import ua.nure.beizerov.practice7.entity.Aircraft;
import ua.nure.beizerov.practice7.entity.Chars;
import ua.nure.beizerov.practice7.entity.Parameters;
import ua.nure.beizerov.practice7.entity.Plane;
import ua.nure.beizerov.practice7.entity.Type;


/**
 * Controller for StAX parser.
 * 
 * @author Alexei Beizerov
 * @version 1.0
 */
public class STAXController extends DefaultHandler {
	
	private String xmlFileName;
	
	// main container
	private Aircraft aircraft;
	
	
	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	
	
	public Aircraft getAircraft() {
		return aircraft;
	}
	
	/**
	 * Parses XML document with StAX (based on event reader). There is no validation during the
	 * parsing.
	 */
	public void parse() throws XMLStreamException {
		Plane plane = null;
		
		// current element name holder
		String currentElement = null;
		
		XMLInputFactory factory = XMLInputFactory.newInstance();
		
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(
				new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			// skip any empty content
			if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
				continue;
			}

			// handler for start tags
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();

				if (Names.AIRCRAFT.equals(currentElement)) {
					aircraft = new Aircraft();
					continue;
				}

				if (Names.PLANE.equals(currentElement)) {
					plane = new Plane();
					continue;
				}
				
				if (Names.CHARS.equals(currentElement)) {
					plane.setChars(new Chars());
					Attribute attribute = startElement.getAttributeByName(
							new QName(Names.RADAR)
					);
					
					if (attribute != null) {
						plane.getChars().setRadar(
							Boolean.parseBoolean(attribute.getValue())
						);
					}
					
					continue;
				}
				
				switch (currentElement) {
				case "Support":
					plane.getChars().setType(Type.SUPPORT);
					break;
				case "Escort":
					plane.getChars().setType(Type.ESCORT);
					break;
				case "Fighter":
					plane.getChars().setType(Type.FIGHTER);
					break;
				case "Interceptor":
					plane.getChars().setType(Type.INTERCEPTOR);
					break;
				case "Scout":
					plane.getChars().setType(Type.SCOUT);
					break;
				default:
					break;
				}
				
				if (Names.PARAMETERS.equals(currentElement)) {
					plane.setParameters(new Parameters());
					continue;
				}
			}

			// handler for contents
			if (event.isCharacters()) {
				Characters characters = event.asCharacters();

				if (Names.MODEL.equals(currentElement)) {
					plane.setModel(characters.getData());
					continue;
				}
				
				if (Names.ORIGIN.equals(currentElement)) {
					plane.setOrigin(characters.getData());
					continue;
				}
				
				if (Names.MISSILE.equals(currentElement)) {
					plane.getChars().setMissiles(
							(Integer.parseInt(characters.getData()))
					);
					continue;
				}
				
				if (Names.PLACES.equals(currentElement)) {
					plane.getChars().setPlaces(
							(Integer.parseInt(characters.getData()))
					);
					continue;
				}
				
				if (Names.LENGTH.equals(currentElement)) {
					plane.getParameters().setLength(
							(Double.parseDouble(characters.getData()))
					);
					continue;
				}
				
				if (Names.WIDTH.equals(currentElement)) {
					plane.getParameters().setWidth(
							(Double.parseDouble(characters.getData()))
					);
					continue;
				}
				
				if (Names.HEIGHT.equals(currentElement)) {
					plane.getParameters().setHeight(
							(Double.parseDouble(characters.getData()))
					);
					continue;
				}
				
				if (Names.PRICE.equals(currentElement)) {
					plane.setPrice(Double.parseDouble(characters.getData()));
					continue;
				}
			}

			// handler for end tags
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();

				if (Names.MODEL.equals(localName)) {
					aircraft.getPlanes().add(plane);
					continue;
				}
			}
		}
		
		reader.close();
	}
	
	public static void main(String[] args) throws Exception {
		// try to parse (valid) XML file (success)
		STAXController staxContr = new STAXController(Constants.VALID_XML_FILE);
		staxContr.parse(); // <-- do parse (success)

		// obtain container
		Aircraft aircraft = staxContr.getAircraft();

		// we have Aircraft object at this point:
		System.out.println("====================================");
		System.out.print("\t*** Aircraft ***\n" + aircraft);
		System.out.println("====================================");
	}
}
