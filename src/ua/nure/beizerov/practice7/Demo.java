package ua.nure.beizerov.practice7;


/**
 * Demo class to run the project without command line arguments.
 *
 * @author Alexei Beizerov
 * @version 1.0
 */
public class Demo {

	public static void main(String[] args) throws Exception {
		Main.main(new String[] { "input.xml" });
	}
}
